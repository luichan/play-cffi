void evolve(int Nx, int Ny, double in[][Ny], double out[][Ny], double D, double dt) {
    int i, j;
    double laplacian;
    for (i=1; i<Nx-1; i++) {
        for (j=1; j<Ny-1; j++) {
            laplacian = in[i+1][j] + in[i-1][j] + in[i][j+1] + in[i][j-1] - 4 * in[i][j];
            out[i][j] = in[i][j] + D * dt * laplacian;
        }
    }
}

void fill_buffer(char *buf)
{
    static int read_count = 0;
    int ii;
    read_count ++;
    for (ii=0; ii<read_count; ii++)
    	buf[ii]='A';
}

void increment(int *param)
{
	(*param)++;
}
