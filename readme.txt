************************************************************************
* Description
************************************************************************
Example code based on High Performance Python book, to play with Python
calling C lib code

************************************************************************
* Required Tools
************************************************************************
Python 3
CFFI
If you are using Windows, install cygwin

************************************************************************
* File/Folder description
************************************************************************
myutil.c:
   C lib source code

myapi.c:
   C lib source code which acts as a API for myutil lib
   
main.c:
   C main code that calls myapi lib

test.py
   Python code that calls myapi lib

test_inline.py
   Python code that calls inline C code
   

************************************************************************
* Build instruction for Windows using cygwin
************************************************************************

$ export PATH=$PATH:/cygdrive/d/py_ws/play-cffi

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ gcc -c -Wall -Werror myutil.c

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ gcc -shared -o libmyutil.so myutil.o

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ ln -s /cygdrive/d/py_ws/play-cffi/libmyutil.so libmyutil.dll

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ gcc -std=gnu99 -c myapi.c

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ gcc -L/cygdrive/d/py_ws/play-cffi/ -shared -o libmyapi.so myapi.o -lmyutil

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ ln -s /cygdrive/d/py_ws/play-cffi/libmyapi.so libmyapi.dll

luichan@HMI-LuiChan /cygdrive/d/py_ws/play-cffi
$ gcc -L/cygdrive/d/py_ws/play-cffi/ -Wall main.c -o main -lmyutil -lmyapi

======= Run on Windows using command prompt======
D:\py_ws\play-cffi>main
val=15

D:\py_ws\play-cffi>python test.py
0.9600958824157715
[65  0  0  0  0  0  0  0  0  0]
[65 65  0  0  0  0  0  0  0  0]
[65 65 65  0  0  0  0  0  0  0]
[65 65 65 65  0  0  0  0  0  0]
[65 65 65 65 65  0  0  0  0  0]
Now counter= 1
Now counter= 2
Now counter= 3
Now counter= 4
Now counter= 5

D:\py_ws\play-cffi>python test_inline.py
cl : Command line warning D9002 : ignoring unknown option '-O3'
_cffi__x512d80d6x2c9690c5.c
_cffi__x512d80d6x2c9690c5.obj : warning LNK4197: export 'PyInit__cffi__x512d80d6
x2c9690c5' specified multiple times; using first specification
   Creating library D:\py_ws\play-cffi\__pycache__\Release\__pycache__\_cffi__x5
12d80d6x2c9690c5.lib and object D:\py_ws\play-cffi\__pycache__\Release\__pycache
__\_cffi__x512d80d6x2c9690c5.exp
[65  0  0  0  0  0  0  0  0  0]
[65 65  0  0  0  0  0  0  0  0]
[65 65 65  0  0  0  0  0  0  0]
[65 65 65 65  0  0  0  0  0  0]
[65 65 65 65 65  0  0  0  0  0]
Now counter= 1
Now counter= 2
Now counter= 3
Now counter= 4
Now counter= 5

