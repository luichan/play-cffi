import numpy as np
import time
from cffi import FFI, verifier

grid_shape = (512, 512)

ffi = FFI()
ffi.cdef(
    r'''
    void fill_buffer(char *buf);
    void increment(int *param);
    ''')
lib = ffi.verify(r'''
void fill_buffer(char *buf)
{
    static int read_count = 0;
    int ii;
    read_count ++;
    for (ii=0; ii<read_count; ii++)
        buf[ii]='A';
}
void increment(int *param)
{
    (*param)++;
}
''', extra_compile_args=["-O3", ])


if __name__ == "__main__":
    buf_array=np.zeros(10, np.uint8)
    buffer = ffi.cast('char *', buf_array.ctypes.data)
    for ii in range(5):
        lib.fill_buffer(buffer)
        print(buf_array)

    counter = ffi.new('int *')
    counter[0] = 0
    for ii in range(5):
        lib.increment(counter)
        print('Now counter=', counter[0])
    verifier.cleanup_tmpdir()
